package bowlingramp.contender;

import bowlingramp.window.Window;
import junit.framework.Assert;
import org.junit.Test;
import bowlingramp.window.LastWindow;

import java.util.ArrayList;
import java.util.List;

public class ContenderScoreCalculatorTest {

    private ContenderPointCalculator contenderPointCalculator;

    @Test
    public void testupdatestrikesorspares() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);
        contenderPointCalculator = new ContenderPointCalculator(windows);

        Window currentWindow = createNewWindow(1, 3, 1);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 4;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(2, 0, 3);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 7;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(3, 1, 8);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 16;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(4, 1, 8);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 25;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(5, 1, 5);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 31;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(6, 1, 5);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 37;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(7, 1, 5);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 43;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(8, 1, 5);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 49;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(9, 1, 5);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 55;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(10, 1, 5, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 61;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }


    @Test
    public void testPointSpare() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);

        contenderPointCalculator = new ContenderPointCalculator(windows);
        Window currentWindow = createNewWindow(1, 3, 1);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 4;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        contenderPointCalculator = new ContenderPointCalculator(windows);
        currentWindow = createNewWindow(2, 5, 5);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 4;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        contenderPointCalculator = new ContenderPointCalculator(windows);
        currentWindow = createNewWindow(3, 2, 0);
        windows.add(currentWindow);
        expectedScore = 18;
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    @Test
    public void testScoreWithStrike() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);

        contenderPointCalculator = new ContenderPointCalculator(windows);
        Window currentWindow = createNewWindow(1, 3, 1);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 4;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(2, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 4;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(3, 2, 7);
        windows.add(currentWindow);
        expectedScore = 32;
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    @Test
    public void testPointWithSpareStrike() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);
        contenderPointCalculator = new ContenderPointCalculator(windows);

        Window currentWindow = createNewWindow(1, 4, 6);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(2, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 20;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(3, 2, 7);
        windows.add(currentWindow);
        expectedScore = 48;
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    @Test
    public void testScoreWithStrikeSpare() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);
        contenderPointCalculator = new ContenderPointCalculator(windows);

        Window currentWindow = createNewWindow(1, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(2, 2, 8);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 20;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(3, 2, 7);
        windows.add(currentWindow);
        expectedScore = 41;
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    @Test
    public void testTwoStrikes() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);
        contenderPointCalculator = new ContenderPointCalculator(windows);

        Window currentWindow = createNewWindow(1, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(2, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 0;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(3, 2, 7);
        windows.add(currentWindow);
        expectedScore = 50;
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    @Test
    public void testThreeStrikes() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);
        contenderPointCalculator = new ContenderPointCalculator(windows);

        Window currentWindow = createNewWindow(1, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(2, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 0;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(3, 10, null);
        windows.add(currentWindow);
        expectedScore = 30;
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());

        currentWindow = createNewWindow(4, 3, 6);
        windows.add(currentWindow);
        expectedScore = 81;
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    @Test
    public void testThreeSpares() throws Exception {
        List<Window> windows = new ArrayList<Window>(10);
        contenderPointCalculator = new ContenderPointCalculator(windows);

        Window currentWindow = createNewWindow(1, 3, 7);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(2, 5, 5);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 15;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());


        currentWindow = createNewWindow(3, 6, 4);
        windows.add(currentWindow);

        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 31;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());

        currentWindow = createNewWindow(4, 3, 6);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 53;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    @Test
    public void testTenthFrame() {

        List<Window> windows = new ArrayList<Window>(10);
        contenderPointCalculator = new ContenderPointCalculator(windows);

        Integer numberOfWindows = 6;
        Integer firstBowl = 2;
        Integer secondBowl = 3;

        createPreviousWindows(windows, numberOfWindows, firstBowl, secondBowl);
        Integer expectedScore = 30;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());

        Window currentWindow = createNewWindow(7, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 30;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());

        currentWindow = createNewWindow(8, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 30;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());

        currentWindow = createNewWindow(9, 10, null);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 60;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());

        currentWindow = createNewWindow(10, 10, 10, 10);
        windows.add(currentWindow);
        contenderPointCalculator.updateLastWindowPoints(currentWindow);
        expectedScore = 150;
        Assert.assertEquals(expectedScore, contenderPointCalculator.getLatestPoint());
    }

    private void createPreviousWindows(List<Window> windows, Integer numberOfWindows, Integer firstBowl, Integer secondBowl) {

        for (int i = 1; i <= numberOfWindows; i++) {
            Window currentWindow = createNewWindow(i, firstBowl, secondBowl);
            windows.add(currentWindow);
            contenderPointCalculator.updateLastWindowPoints(currentWindow);
        }
    }

    private Window createNewWindow(Integer windowNumber, Integer firstBall, Integer secondBall) {
        Window currentWindow = new Window(windowNumber);
        currentWindow.submitBallPoint(firstBall);
        if (secondBall != null) {
            currentWindow.submitBallPoint(secondBall);
        }
        return currentWindow;
    }

    private Window createNewWindow(Integer windowNumber, Integer firstBall, Integer secondBall, Integer thirdBall) {
        if (windowNumber != 10) {
            throw new IllegalArgumentException("wrong window number");
        }

        Window currentWindow = new LastWindow();
        currentWindow.submitBallPoint(firstBall);
        currentWindow.submitBallPoint(secondBall);
        if (thirdBall != null) {
            currentWindow.submitBallPoint(thirdBall);
        }
        return currentWindow;
    }
}
