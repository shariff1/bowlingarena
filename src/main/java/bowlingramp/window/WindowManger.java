package bowlingramp.window;

public class WindowManger {
    public Window createWindow(Integer newWindowNumber) {
        if(newWindowNumber ==10){
            return new LastWindow();
        }
        else if(newWindowNumber<10 && newWindowNumber>0){
            return new Window(newWindowNumber);
        }
        else {
            throw new WindowException("New Window number out of bounds - newWindowNumber:"+newWindowNumber);
        }
    }
}
