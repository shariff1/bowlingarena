package bowlingramp;

import org.junit.Assert;
import org.junit.Test;
import bowlingramp.sport.Sport;

public class BowlingAllySportsTest {
    private static final int CONTENDER_1 = 0;
    private static final int CONTENDER_2 = 1;



    @Test
    public void testCheckNameIsUnique() {
        Sport sport = new Sport();
        sport.addContender(1, "Ahmed Shariff");
        Assert.assertFalse(sport.checkNameIsUnique("Ahmed Shariff"));
        Assert.assertTrue(sport.checkNameIsUnique("Asmath somroo"));
    }

    @Test
    public void testSubmitScore() {
        Sport sport = new Sport();
        sport.addContender(1, "Ahmed Shariff");
        sport.addContender(2, "Gautam Kumar");

        //window 1
        sport.submitPoint(CONTENDER_1, 5);
        Integer expectedPoint = 0;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));
        sport.submitPoint(CONTENDER_1, 5);
        expectedPoint = 0;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 3);
        sport.submitPoint(CONTENDER_2, 6);
        expectedPoint = 9;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));


        //window 2
        sport.submitPoint(CONTENDER_1, 5);
        sport.submitPoint(CONTENDER_1, 4);
        expectedPoint = 24;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 5);
        sport.submitPoint(CONTENDER_2, 2);
        expectedPoint = 16;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));

        //window 3
        sport.submitPoint(CONTENDER_1, 5);
        sport.submitPoint(CONTENDER_1, 3);
        expectedPoint = 32;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 10);
        expectedPoint = 16;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));

        //window 4
        sport.submitPoint(CONTENDER_1, 5);
        sport.submitPoint(CONTENDER_1, 3);
        expectedPoint = 40;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 5);
        sport.submitPoint(CONTENDER_2, 1);
        expectedPoint = 38;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));

        //window 5
        sport.submitPoint(CONTENDER_1, 0);
        sport.submitPoint(CONTENDER_1, 0);
        expectedPoint = 40;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 5);
        sport.submitPoint(CONTENDER_2, 1);
        expectedPoint = 44;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));

        //window 6
        sport.submitPoint(CONTENDER_1, 4);
        sport.submitPoint(CONTENDER_1, 3);
        expectedPoint = 47;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 1);
        sport.submitPoint(CONTENDER_2, 3);
        expectedPoint = 48;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));

        //window 7
        sport.submitPoint(CONTENDER_1, 4);
        sport.submitPoint(CONTENDER_1, 4);
        expectedPoint = 55;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 3);
        sport.submitPoint(CONTENDER_2, 3);
        expectedPoint = 54;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));


        //window 8
        sport.submitPoint(CONTENDER_1, 0);
        sport.submitPoint(CONTENDER_1, 2);
        expectedPoint = 57;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 1);
        sport.submitPoint(CONTENDER_2, 9);
        expectedPoint = 54;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));


        //window 9
        sport.submitPoint(CONTENDER_1, 6);
        sport.submitPoint(CONTENDER_1, 2);
        expectedPoint = 65;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 1);
        sport.submitPoint(CONTENDER_2, 6);
        expectedPoint = 72;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));


        //window 10
        sport.submitPoint(CONTENDER_1, 6);
        sport.submitPoint(CONTENDER_1, 2);
        expectedPoint = 73;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_1));


        sport.submitPoint(CONTENDER_2, 9);
        sport.submitPoint(CONTENDER_2, 1);
        sport.submitPoint(CONTENDER_2, 3);
        expectedPoint = 85;
        Assert.assertEquals(expectedPoint, sport.findPoint(CONTENDER_2));

    }

}
