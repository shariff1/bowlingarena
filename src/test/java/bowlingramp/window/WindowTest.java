package bowlingramp.window;

import junit.framework.Assert;
import org.junit.Test;

public class WindowTest {
    @Test
    public void testCorrectBallPoint() throws Exception {

        Window window = new Window(1);
        window.submitBallPoint(0);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(0);
        Integer expectedWindowPoint = 0;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());


        window = new Window(1);
        window.submitBallPoint(1);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 2;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());


        window = new Window(1);
        window.submitBallPoint(2);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 3;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());

        window = new Window(1);
        window.submitBallPoint(3);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 4;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());

        window = new Window(1);
        window.submitBallPoint(4);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 5;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());

        window = new Window(1);
        window.submitBallPoint(5);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 6;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());

        window = new Window(1);
        window.submitBallPoint(6);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 7;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());

        window = new Window(1);
        window.submitBallPoint(7);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 8;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());

        window = new Window(1);
        window.submitBallPoint(8);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        expectedWindowPoint = 9;
        Assert.assertEquals(expectedWindowPoint, window.getWindowPoints().getTotalPoints());

        window = new Window(1);
        window.submitBallPoint(9);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        Assert.assertTrue(window.getWindowPoints().isASpare());

        window = new Window(1);
        window.submitBallPoint(1);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(9);
        Assert.assertTrue(window.getWindowPoints().isASpare());

        window = new Window(1);
        window.submitBallPoint(5);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(5);
        Assert.assertTrue(window.getWindowPoints().isASpare());

        window = new Window(1);
        window.submitBallPoint(0);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(10);
        Assert.assertTrue(window.getWindowPoints().isASpare());

        window = new Window(1);
        window.submitBallPoint(10);
        Assert.assertTrue(window.getWindowPoints().isABigStrike());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncorrectLowBallPoint() throws Exception {

        Window window = new Window(1);
        window.submitBallPoint(1);
        window.submitBallPoint(1);
        window.submitBallPoint(1);
    }

    @Test(expected = WindowPointException.class)
    public void testIncorrectHighBallPoint() throws Exception {
        Window window = new Window(1);
        window.submitBallPoint(5);
        window.submitBallPoint(7);
    }

    @Test
    public void isWindowInPlay() {
        Window window = new Window(1);
        window.submitBallPoint(1);
        Assert.assertTrue(window.isWindowInPlay());
        window.submitBallPoint(1);
        Assert.assertFalse(window.isWindowInPlay());

        window = new Window(1);
        window.submitBallPoint(5);
        window.submitBallPoint(5);
        Assert.assertFalse(window.isWindowInPlay());

        window = new Window(1);
        window.submitBallPoint(10);
        Assert.assertFalse(window.isWindowInPlay());

        window = new Window(1);
        window.submitBallPoint(0);
        window.submitBallPoint(10);
        Assert.assertFalse(window.isWindowInPlay());
    }

}
