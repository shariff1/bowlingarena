package bowlingramp.contender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import bowlingramp.window.Window;
import bowlingramp.window.WindowManger;
import bowlingramp.window.WindowPoints;
import bowlingramp.sport.SportChecker;

import java.util.ArrayList;
import java.util.List;

public class Contender {
    private Integer id;
    private List<Window> windows = new ArrayList<Window>(10);
    private ContenderPointCalculator contenderPointCalculator;
    private WindowManger windowManger = new WindowManger();
    private SportChecker validator = new SportChecker();
    private static final int CURRENT_WINDOW_OFFSET = 1;
    private final Logger logger = LoggerFactory.getLogger(Contender.class);
    private String name;

    public Contender(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public boolean addBowl(Integer point) {
        checkPoints(point);
        Integer currentWindowNumber = windows.size();

            Window currentWindow = null;
            Integer windownumbers = currentWindowNumber + 1;


            if (windownumbers == 1) {
                currentWindow = windowManger.createWindow(windownumbers);
                currentWindow.submitBallPoint(point);
                windows.add(currentWindow);

            } else {
                currentWindow = windows.get(currentWindowNumber - CURRENT_WINDOW_OFFSET);
                if (currentWindow.isWindowInPlay()) {
                    currentWindow.submitBallPoint(point);
                    updatePreviousWindow(currentWindow);
                
                } else {
                    currentWindow = windowManger.createWindow(windownumbers);
                    currentWindow.submitBallPoint(point);
                    windows.add(currentWindow);
                    updatePreviousWindow(currentWindow);

                }

            }
        return currentWindow.isWindowFinished();
    }

    private void checkPoints(Integer point) {
        if (point==null || !validator.validateBallPoint(point.toString())){
              throw new ContenderPointException("contender point invalid - point:" + point);
        }
    }


    private void updatePreviousWindow(Window currentWindow) {
        if (currentWindow.isWindowFinished()) {
            contenderPointCalculator = new ContenderPointCalculator(windows);
            contenderPointCalculator.updateLastWindowPoints(currentWindow);
        }
    }

    public WindowPoints getWindowPoint(int windowNumber) {
        if (windowNumber + 1 <= windows.size()) {
            return windows.get(windowNumber).getWindowPoints();
        }
        return null;
    }

    public Integer getTotalPoints() {
        contenderPointCalculator = new ContenderPointCalculator(windows);
        return contenderPointCalculator.getLatestPoint();
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Contender{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", windows=" + windows +
                '}';
    }
}
