package bowlingramp.sport;

import bowlingramp.contender.Contender;
import bowlingramp.window.WindowPoints;

public class ScoreBoard {
    private Integer sportsPoint = 0;

    public String getScoreBoard(Contender contender) {
        StringBuilder scoreBoard = new StringBuilder();
        scoreBoard.append("\nPLAYER " + contender.getName());
        scoreBoard.append("\n___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________");
        scoreBoard.append("\n|    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    |");
        scoreBoard.append("\n| " + getBallPoint(contender, 0, 1) + " | " + getBallPoint(contender, 0, 2) + " | | " + getBallPoint(contender, 1, 1) + " | " + getBallPoint(contender, 1, 2) + " | | " + getBallPoint(contender, 2, 1) + " | " + getBallPoint(contender, 2, 2) + " | | " + getBallPoint(contender, 3, 1) + " | " + getBallPoint(contender, 3, 2) + " | | " + getBallPoint(contender, 4, 1) + " | " + getBallPoint(contender, 4, 2) + " | | " + getBallPoint(contender, 5, 1) + " | " + getBallPoint(contender, 5, 2) + " | | " + getBallPoint(contender, 6, 1) + " | " + getBallPoint(contender, 6, 2) + " | | " + getBallPoint(contender, 7, 1) + " | " + getBallPoint(contender, 7, 2) + " | | " + getBallPoint(contender, 8, 1) + " | " + getBallPoint(contender, 8, 2) + " | | " + getBallPoint(contender, 9, 1) + " | " + getBallPoint(contender, 9, 2) + " |");
        scoreBoard.append("\n|    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____|");
        scoreBoard.append("\n|         | |         | |         | |         | |         | |         | |         | |         | |         | |         |");
        scoreBoard.append("\n|   " + getSportsPoint(0, contender) + "   | |   " + getSportsPoint(1, contender) + "   | |   " + getSportsPoint(2, contender) + "   | |   " + getSportsPoint(3, contender) + "   | |   " + getSportsPoint(4, contender) + "   | |   " + getSportsPoint(5, contender) + "   | |   " + getSportsPoint(6, contender) + "   | |   " + getSportsPoint(7, contender) + "   | |   " + getSportsPoint(8, contender) + "   | |   " + getSportsPoint(9, contender) + "   |");
        scoreBoard.append("\n|_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________|\n\n");
        return scoreBoard.toString();
    }

    private String getBallPoint(Contender contender, Integer windowNumber, Integer ballNumber) {
        Integer ballPoint = null;
        WindowPoints windowPoints = contender.getWindowPoint(windowNumber);
        if (ballNumber == 1) {
            ballPoint = windowPoints.getFirstBallPoint();
        } else if (ballNumber == 2) {
            ballPoint = contender.getWindowPoint(windowNumber).getSecondBallPoint();
        }

        return formatBallPoint(ballNumber, ballPoint, windowPoints);
    }


    private String formatBallPoint(Integer ballNumber, Integer ballPoint, WindowPoints windowPoints) {
        String point = "";
        if (ballNumber == 1) {
            point = createPointsResult(ballPoint, point);
        } else if (ballNumber == 2) {
            if (windowPoints.isABigStrike()) {
                point += "X";
            } else if (windowPoints.isASpare()) {
                point += "/";
            } else point = createPointsResult(ballPoint, point);
        }
        point = String.format("%2s", point);
        return point;
    }

    private String createPointsResult(Integer ballPoint, String point) {
        if (ballPoint == null) {
            point += "-";
        } else {
            point += ballPoint.toString();
        }
        return point;
    }


    private String getSportsPoint(int windowsNumbers, Contender contender) {
        String point = "";
        Integer totalPoint = null;
        WindowPoints windowPoints = contender.getWindowPoint(windowsNumbers);
        if (windowPoints != null) {
            totalPoint = windowPoints.getTotalPoints();
        }

        if (totalPoint != null) {
            sportsPoint += totalPoint;
            point = sportsPoint.toString();
        } else {
            point = "-";
        }
        point = String.format("%3s", point);
        return point;
    }

}
