package bowlingramp.sport;

import bowlingramp.contender.Contender;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SportScoresCalculatorTest {
    private static final int CONTENDER_1_ID = 1;
    private static final int CONTENDER_2_ID = 2;
    private static final int CONTENDER_3_ID = 3;
    private static final String CONTENDER_1_NAME = "Ahmed Shariff";
    private static final String CONTENDER_2_NAME = "Gautam Kumar";
    private static final String CONTENDER_3_NAME = "Asmath somroo";
    private SportCalculator sportCalculator;

    @Test
    public void testOneWinner() {
        List<Contender> contenders = contenderTwoWins();
        sportCalculator = new SportCalculator(contenders);

        String expectedWinner = "Contender " + CONTENDER_2_ID + ": " + CONTENDER_2_NAME;
        Assert.assertEquals(expectedWinner, sportCalculator.determineWinner().get(0));

    }

    @Test
    public void testTwoWinners() {
        List<Contender> contenders = contenderOneandContenderThree();
        sportCalculator = new SportCalculator(contenders);

        List<String> expectedWinners = new ArrayList<String>();
        String expectedWinner1 = "Contender " + CONTENDER_1_ID + ": " + CONTENDER_1_NAME;
        String expectedWinner2 = "Contender " + CONTENDER_3_ID + ": " + CONTENDER_3_NAME;
        expectedWinners.add(expectedWinner1);
        expectedWinners.add(expectedWinner2);


        Assert.assertEquals(expectedWinners, sportCalculator.determineWinner());

    }

    @Test
    public void teamScoreTest() {
        List<Contender> contenders = setUpContendersForFinishedGame();
        sportCalculator = new SportCalculator(contenders);
        Integer expectedScore = 120;
        Assert.assertEquals(expectedScore, sportCalculator.determineTeamPoint());

    }

    private List<Contender> contenderTwoWins() {
        List<Contender> contenders = new ArrayList<Contender>();
        Contender contender1 = new Contender(CONTENDER_1_ID, CONTENDER_1_NAME);
        addScores(contender1, 1);
        Contender contender2 = new Contender(CONTENDER_2_ID, CONTENDER_2_NAME);
        addScores(contender2, 4);
        Contender contender3 = new Contender(CONTENDER_3_ID, CONTENDER_3_NAME);
        addScores(contender3, 3);

        contenders.add(contender1);
        contenders.add(contender2);
        contenders.add(contender3);
        return contenders;

    }

    private List<Contender> contenderOneandContenderThree() {
        List<Contender> contenders = new ArrayList<Contender>();
        Contender contender1 = new Contender(CONTENDER_1_ID, CONTENDER_1_NAME);
        addScores(contender1, 4);
        Contender contender2 = new Contender(CONTENDER_2_ID, CONTENDER_2_NAME);
        addScores(contender2, 1);
        Contender contender3 = new Contender(CONTENDER_3_ID, CONTENDER_3_NAME);
        addScores(contender3, 4);

        contenders.add(contender1);
        contenders.add(contender2);
        contenders.add(contender3);
        return contenders;
    }

    private List<Contender> setUpContendersForFinishedGame() {

        List<Contender> contenders = new ArrayList<Contender>();
        Contender contender1 = new Contender(CONTENDER_1_ID, CONTENDER_1_NAME);
        addScores(contender1, 1);
        Contender contender2 = new Contender(CONTENDER_2_ID, CONTENDER_2_NAME);
        addScores(contender2, 2);
        Contender contender3 = new Contender(CONTENDER_3_ID, CONTENDER_3_NAME);
        addScores(contender3, 3);

        contenders.add(contender1);
        contenders.add(contender2);
        contenders.add(contender3);
        return contenders;
    }

    private void addScores(Contender contender, int ballScore) {
        if (ballScore <= 4) {
            for (int i = 1; i <= 20; i++) {
                contender.addBowl(ballScore);
            }
        }
    }
}
