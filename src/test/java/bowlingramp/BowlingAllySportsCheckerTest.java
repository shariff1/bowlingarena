package bowlingramp;

import junit.framework.Assert;
import org.junit.Test;
import bowlingramp.sport.SportChecker;

public class BowlingAllySportsCheckerTest {
    private SportChecker sportChecker = new SportChecker();


    @Test
    public void testCheckName() {
        Assert.assertFalse(sportChecker.validateName(""));
        Assert.assertFalse(sportChecker.validateName(" "));
        Assert.assertTrue(sportChecker.validateName("A"));
        Assert.assertTrue(sportChecker.validateName("Ahm"));
        Assert.assertTrue(sportChecker.validateName("Ahmed"));
        Assert.assertTrue(sportChecker.validateName("Ahmed Shariff"));
        Assert.assertTrue(sportChecker.validateName("Ahmed Shariff1"));
    }

    @Test
    public void testCheckNumberBetween1And10() {
        for (Integer i = 0; i <= 10; i++) {
            Assert.assertTrue(sportChecker.validateBallPoint(i.toString()));
        }
        Assert.assertFalse(sportChecker.validateBallPoint("-1"));
        Assert.assertFalse(sportChecker.validateBallPoint("11"));
    }

    @Test
    public void testCheckNumberGreaterThanZero() {
        Assert.assertFalse(sportChecker.validateNumberOfContenders("0"));
        Assert.assertFalse(sportChecker.validateNumberOfContenders("-1"));
        Assert.assertTrue(sportChecker.validateNumberOfContenders("1"));
        Assert.assertTrue(sportChecker.validateNumberOfContenders("10"));
    }
}
