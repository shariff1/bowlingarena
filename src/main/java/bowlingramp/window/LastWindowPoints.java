package bowlingramp.window;

public class LastWindowPoints extends WindowPoints {

    private  Integer thirdBallPoint;

    public Integer getThirdBallPoint() {
        return thirdBallPoint;
    }

    public void setThirdBallPoint(Integer thirdBallScore) {
        this.thirdBallPoint = thirdBallScore;
    }

    public boolean hasDoubleStrike() {
        return getFirstBallPoint() == 10 && getSecondBallPoint() == 10;
    }

     public boolean hasSpare() {
        return ((getFirstBallPoint() + getSecondBallPoint() == 10) && !isFirstBallAStrike());
    }

     public boolean isFirstBallAStrike() {
        return getFirstBallPoint() == 10 ;
    }


    public Integer addPoints() {

        Integer totalPoint = getFirstBallPoint();
        if (getSecondBallPoint() != null) {
            totalPoint = totalPoint + getSecondBallPoint();
        }
        if (thirdBallPoint != null) {
            totalPoint = totalPoint + thirdBallPoint;
        }
        return totalPoint;
    }

    public boolean hasSecondBallStrike() {
        return getSecondBallPoint()!=null && getSecondBallPoint()==10;
    }
}
