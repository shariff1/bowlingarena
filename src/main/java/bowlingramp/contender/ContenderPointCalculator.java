package bowlingramp.contender;

import bowlingramp.window.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ContenderPointCalculator {

    private final Logger logger = LoggerFactory.getLogger(ContenderPointCalculator.class);
    private List<Window> windows;
    private static final int PREVIOUS_WINDOW_OFFSET = 2;

    public ContenderPointCalculator(List<Window> windows) {
        this.windows = windows;
    }

    public void updateLastWindowPoints(Window currentWindow) {
        Window previousWindow = getLastWindow(currentWindow);
        calculateForLastWindow(getLastWindow(previousWindow), previousWindow, currentWindow);
    }

    private Window getLastWindow(Window currentWindow) {
        Window previousWindow = null;
        if (currentWindow != null && currentWindow.getWindowNumber() > 1) {
            previousWindow = windows.get(currentWindow.getWindowNumber() - PREVIOUS_WINDOW_OFFSET);
        }
        return previousWindow;
    }

    public Integer getLatestPoint() {
        Integer currentPoints = 0;
        for (Window window : windows) {
            logger.debug("getLatestPoint() window number:{} window score:{} ", window.getWindowNumber(), window.getWindowPoints());
            if (window.getWindowPoints().getTotalPoints() != null) {
                currentPoints = currentPoints + window.getWindowPoints().getTotalPoints();
                logger.debug("getLatestPoint() {} ", currentPoints);
            }
        }
        logger.debug("getLatestPoint() total:{}", currentPoints);
        return currentPoints;
    }

    private void calculateForLastWindow(Window previousPreviousWindow, Window previousWindow, Window currentWindow) {
        if (previousPreviousWindow != null && !previousPreviousWindow.isWindowComplete()) {
            calculateLastLastWindowPoint(previousPreviousWindow, previousWindow, currentWindow);
        }

        if (currentWindow.getWindowNumber() != 10) {
            if (previousWindow != null && !previousWindow.isWindowComplete()) {
                calculateLastsWindowPoint(previousWindow, currentWindow);
            }
        } else {
            calculateLastWindowCounntWhenCurrentWindowIsLast(previousWindow, currentWindow);
        }


    }

    private void calculateLastLastWindowPoint(Window previousPreviousWindow, Window previousWindow, Window currentWindow) {
        if (previousPreviousWindow.getWindowPoints().isABigStrike() && previousWindow.getWindowPoints().isABigStrike()) {
            Integer totalScore = previousPreviousWindow.getWindowPoints().addPoints() + previousWindow.getWindowPoints().addPoints() + currentWindow.getWindowPoints().getFirstBallPoint();
            previousPreviousWindow.getWindowPoints().setTotalPoints(totalScore);
        }
    }

    private void calculateLastsWindowPoint(Window previousWindow, Window currentWindow) {
        if (previousWindow.getWindowPoints().isABigStrike() && !currentWindow.getWindowPoints().isABigStrike()) {
            Integer totalScore = previousWindow.getWindowPoints().addPoints() + currentWindow.getWindowPoints().addPoints();
            previousWindow.getWindowPoints().setTotalPoints(totalScore);
        } else if (previousWindow.getWindowPoints().isASpare()) {
            Integer totalScore = previousWindow.getWindowPoints().addPoints() + currentWindow.getWindowPoints().getFirstBallPoint();
            previousWindow.getWindowPoints().setTotalPoints(totalScore);
        }
    }

    private void calculateLastWindowCounntWhenCurrentWindowIsLast(Window previousWindow, Window currentWindow) {
        if (previousWindow.getWindowPoints().isABigStrike()) {
            Integer totalScore = previousWindow.getWindowPoints().addPoints() + currentWindow.getWindowPoints().getFirstBallPoint() + currentWindow.getWindowPoints().getSecondBallPoint();
            previousWindow.getWindowPoints().setTotalPoints(totalScore);
        } else if (previousWindow.getWindowPoints().isASpare()) {
            Integer totalScore = previousWindow.getWindowPoints().addPoints() + currentWindow.getWindowPoints().getFirstBallPoint();
            previousWindow.getWindowPoints().setTotalPoints(totalScore);
        }
    }
}
