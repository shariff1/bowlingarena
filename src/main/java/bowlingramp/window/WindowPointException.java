package bowlingramp.window;

public class WindowPointException extends RuntimeException {
    public WindowPointException(String text) {
        super(text);
    }
}
