package bowlingramp.contender;

import org.junit.Assert;
import org.junit.Test;

public class ContenderScoreTest {

    private Contender contender;

    @Test
    public void testAddPoints() throws Exception {
        contender = new Contender(1, "Ahmed Shariff");

        //window 1
        contender.addBowl(1);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());
        contender.addBowl(0);
        expectedScore = 1;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 2
        contender.addBowl(1);
        Assert.assertEquals(expectedScore, contender.getTotalPoints());
        contender.addBowl(5);
        expectedScore = 7;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 3
        contender.addBowl(5);
        contender.addBowl(5);
        expectedScore = 7;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 4
        contender.addBowl(4);
        contender.addBowl(5);
        expectedScore = 30;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 5
        contender.addBowl(10);
        expectedScore = 30;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 6
        contender.addBowl(4);
        contender.addBowl(6);
        expectedScore = 50;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());


        //window 7
        contender.addBowl(10);
        expectedScore = 70;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 8
        contender.addBowl(3);
        contender.addBowl(5);
        expectedScore = 96;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 9
        contender.addBowl(10);
        expectedScore = 96;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 10
        contender.addBowl(3);
        contender.addBowl(4);
        expectedScore = 120;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());
    }

    @Test
    public void testExtraBowlInlastWindow() throws Exception {
        contender = new Contender(1, "Gautam Kumar");

        //window 1
        contender.addBowl(1);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());
        contender.addBowl(0);
        expectedScore = 1;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 2
        contender.addBowl(1);
        Assert.assertEquals(expectedScore, contender.getTotalPoints());
        contender.addBowl(5);
        expectedScore = 7;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 3
        contender.addBowl(5);
        contender.addBowl(5);
        expectedScore = 7;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 4
        contender.addBowl(4);
        contender.addBowl(5);
        expectedScore = 30;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 5
        contender.addBowl(10);
        expectedScore = 30;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 6
        contender.addBowl(4);
        contender.addBowl(6);
        expectedScore = 50;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());


        //window 7
        contender.addBowl(10);
        expectedScore = 70;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 8
        contender.addBowl(3);
        contender.addBowl(5);
        expectedScore = 96;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 9
        contender.addBowl(5);
        contender.addBowl(4);
        expectedScore = 105;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 10
        contender.addBowl(6);
        contender.addBowl(4);
        contender.addBowl(6);
        expectedScore = 121;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

    }

    @Test
    public void testFinalPoints() throws Exception {
        contender = new Contender(1, "Asmath somroo");

        //window 1
        contender.addBowl(10);
        Integer expectedScore = 0;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 2
        contender.addBowl(10);
        expectedScore = 0;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 3
        contender.addBowl(10);
        expectedScore = 30;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 4
        contender.addBowl(10);
        expectedScore = 60;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 5
        contender.addBowl(10);
        expectedScore = 90;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 6
        contender.addBowl(10);
        expectedScore = 120;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());


        //window 7
        contender.addBowl(10);
        expectedScore = 150;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 8
        contender.addBowl(10);
        expectedScore = 180;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 9
        contender.addBowl(10);
        expectedScore = 210;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

        //window 10
        contender.addBowl(10);
        contender.addBowl(10);
        contender.addBowl(10);
        expectedScore = 300;
        Assert.assertEquals(expectedScore, contender.getTotalPoints());

    }
}
