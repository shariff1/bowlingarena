package bowlingramp.window;

public class WindowPoints {

    private Integer firstBallPoint;
    private Integer secondBallPoint;
    private Integer totalPoints;

    public Integer addPoints() {
        Integer total = firstBallPoint;
        if (secondBallPoint != null) {
            total = total + secondBallPoint;
        }
        return total;
    }

    public Integer getFirstBallPoint() {
        return firstBallPoint;
    }

    public void setFirstBallPoint(Integer firstBallPoint) {
        this.firstBallPoint = firstBallPoint;
    }

    public Integer getSecondBallPoint() {
        return secondBallPoint;
    }

    public void setSecondBallPoint(Integer secondBallPoint) {
        this.secondBallPoint = secondBallPoint;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    public boolean isABigStrike() {
        return firstBallPoint != null && firstBallPoint == 10;
    }

    public boolean isASpare() {
        return firstBallPoint != null && secondBallPoint != null && ((firstBallPoint + secondBallPoint) == 10);
    }

    @Override
    public String toString() {
        return "WindowPoints{" +
                "firstBallPoint=" + firstBallPoint +
                ", secondBallPoint=" + secondBallPoint +
                ", totalPoints=" + totalPoints +
                '}';
    }
}
