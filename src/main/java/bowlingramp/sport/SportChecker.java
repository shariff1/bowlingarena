package bowlingramp.sport;

public class SportChecker {

    public boolean validateNumberOfContenders(String numberofContenders) {
        return numberofContenders != null && numberofContenders.matches("^[1-9]+[0-9]*$");
    }

    public boolean validateBallPoint(String ballScore) {
        return (ballScore != null && ballScore.matches("^[0-9]|10"));
    }

    public boolean validateName(String name) {
        if (name != null) {
            name = name.trim();
            return name.matches("^[a-zA-Z0-9 ]+$");
        }
        return false;
    }
}
