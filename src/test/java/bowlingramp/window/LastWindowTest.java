package bowlingramp.window;

import junit.framework.Assert;
import org.junit.Test;

public class LastWindowTest {

    @Test
    public void tesForTwoBalls() throws Exception {
        Window window = new LastWindow();
        window.submitBallPoint(0);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        Integer expected = 1;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(9);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(0);
        expected = 9;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());
    }

    @Test
    public void testForThreeBalls() throws Exception {
        Window window = new LastWindow();
        window.submitBallPoint(0);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(10);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(0);
        Assert.assertTrue(window.getWindowPoints().isASpare());

        window = new LastWindow();
        window.submitBallPoint(5);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(5);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(0);
        Assert.assertTrue(window.getWindowPoints().isASpare());

        window = new LastWindow();
        window.submitBallPoint(5);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(5);
        Assert.assertNull(window.getWindowPoints().getTotalPoints());
        window.submitBallPoint(1);
        Integer expected = 11;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(10);
        window.submitBallPoint(10);
        window.submitBallPoint(10);
        expected = 30;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(0);
        window.submitBallPoint(10);
        window.submitBallPoint(10);
        expected = 20;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(0);
        window.submitBallPoint(10);
        window.submitBallPoint(1);
        expected = 11;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(10);
        window.submitBallPoint(0);
        window.submitBallPoint(0);
        expected = 10;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(10);
        window.submitBallPoint(0);
        window.submitBallPoint(1);
        expected = 11;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(10);
        window.submitBallPoint(10);
        window.submitBallPoint(0);
        expected = 20;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(10);
        window.submitBallPoint(10);
        window.submitBallPoint(1);
        expected = 21;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

        window = new LastWindow();
        window.submitBallPoint(1);
        window.submitBallPoint(9);
        window.submitBallPoint(1);
        expected = 11;
        Assert.assertEquals(expected, window.getWindowPoints().getTotalPoints());

    }

    @Test(expected = UnsupportedOperationException.class)
    public void testThirdBallWithLowScores() throws Exception {
        Window window = new LastWindow();
        window.submitBallPoint(0);
        window.submitBallPoint(1);
        window.submitBallPoint(0);
    }

    @Test
    public void isWindowInPlay() {
        Window window = new LastWindow();
        window.submitBallPoint(1);
        Assert.assertTrue(window.isWindowInPlay());
        window.submitBallPoint(1);
        Assert.assertFalse(window.isWindowInPlay());

        window = new LastWindow();
        window.submitBallPoint(5);
        window.submitBallPoint(5);
        Assert.assertTrue(window.isWindowInPlay());

        window = new LastWindow();
        window.submitBallPoint(10);
        window.submitBallPoint(0);
        Assert.assertTrue(window.isWindowInPlay());

        window = new LastWindow();
        window.submitBallPoint(0);
        window.submitBallPoint(10);
        Assert.assertTrue(window.isWindowInPlay());
    }

}
