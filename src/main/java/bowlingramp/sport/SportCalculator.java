package bowlingramp.sport;

import bowlingramp.contender.Contender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SportCalculator {
    private final Logger logger = LoggerFactory.getLogger(SportCalculator.class);
    private List<Contender> contenders;

    public SportCalculator(List<Contender> contenders) {
        this.contenders = contenders;
    }

    public List<String> determineWinner() {
        Integer highestPoint = findHighestPoint();
        return findPlayersWithHighestPoint(highestPoint);
    }

    private List<String> findPlayersWithHighestPoint(Integer highestPoint) {
        List<String> winningContenders = new ArrayList<String>();
        for (Contender contender : contenders) {
                logger.debug("findContender() contenderId: {} Total: {} ", contender.getName(), contender.getTotalPoints());
                if (contender.getTotalPoints().equals(highestPoint)) {
                    winningContenders.add("Contender " + contender.getId() + ": " + contender.getName());
                }

        }
        return winningContenders;
    }

    private Integer findHighestPoint() {
        Integer highestPoint = -1;
        for (Contender contender : contenders) {
            logger.debug("findContender() contenderId: {} Total: {} ", contender.getName(), contender.getTotalPoints());
            if (contender.getTotalPoints() >= highestPoint) {
                highestPoint = contender.getTotalPoints();
            }
        }
        return highestPoint;
    }

    public Integer determineTeamPoint() {
        Integer teamPoint = 0;
        for (Contender contender : contenders) {
            teamPoint = teamPoint + contender.getTotalPoints();
        }
        return teamPoint;
    }


}
