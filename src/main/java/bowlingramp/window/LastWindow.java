package bowlingramp.window;

public class LastWindow extends Window {
    private final LastWindowPoints lastWindowPoints = new LastWindowPoints();
    private boolean hasThirdBall = false;

    public LastWindow() {
        super(10);
    }

    public void submitBallPoint(int ballPoint) {
        if (lastWindowPoints.getFirstBallPoint() == null) {
            handleFirstBall(ballPoint);

        } else if (lastWindowPoints.getSecondBallPoint() == null) {
            handleSecondBall(ballPoint);


        } else if (hasThirdBall && lastWindowPoints.getThirdBallPoint() == null) {
            handleThirdBall(ballPoint);

        } else {
            throw new UnsupportedOperationException("submitBallPoint when window is finished");
        }
    }

    public WindowPoints getWindowPoints() {
        return this.lastWindowPoints;
    }

    public boolean isWindowInPlay() {
        return lastWindowPoints.getTotalPoints() == null && lastWindowPoints.getFirstBallPoint() != null
                && (lastWindowPoints.getSecondBallPoint() == null || (hasThirdBall && lastWindowPoints.getThirdBallPoint() == null));
    }

    private void handleFirstBall(int ballPoint) {
        lastWindowPoints.setFirstBallPoint(ballPoint);
    }

    private void handleSecondBall(int ballPoint) {
        Integer totalScore = lastWindowPoints.getFirstBallPoint() + ballPoint;
        validateTotalPointsForTwoBalls(totalScore, lastWindowPoints.getFirstBallPoint());
        lastWindowPoints.setSecondBallPoint(ballPoint);
        if (lastWindowPoints.isABigStrike() || lastWindowPoints.hasSpare()) {
            hasThirdBall = true;
        } else {
            lastWindowPoints.setTotalPoints(lastWindowPoints.getFirstBallPoint() + lastWindowPoints.getSecondBallPoint());
        }
    }

    private void handleThirdBall(int ballPoint) {
        Integer totalScore = lastWindowPoints.getFirstBallPoint() + lastWindowPoints.getSecondBallPoint() + ballPoint;
        validateTotalPointsForThreeBalls(totalScore);
        lastWindowPoints.setThirdBallPoint(ballPoint);
        lastWindowPoints.setTotalPoints(lastWindowPoints.getFirstBallPoint() + lastWindowPoints.getSecondBallPoint() + lastWindowPoints.getThirdBallPoint());
    }

    private void validateTotalPointsForThreeBalls(Integer totalPoint) {
        if (totalPoint > 30) {
            throw new WindowPointException("Total points greater than maximum possible for 10th Window i.e. 30 - totalPoints:" + totalPoint);
        }
    }

    private void validateTotalPointsForTwoBalls(Integer totalPoint, Integer firstBallPoint) {
        if (firstBallPoint < 10 && totalPoint > 10) {
            throw new WindowPointException("Total point greater than maximum possible i.e. 10 - totalPoint:" + totalPoint);
        }
    }
}
