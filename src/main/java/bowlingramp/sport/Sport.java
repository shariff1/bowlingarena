package bowlingramp.sport;

import bowlingramp.contender.Contender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Sport {

    private final Logger logger = LoggerFactory.getLogger(Sport.class);
    public SportCalculator sportCalculator = null;

    private List<Contender> contenders = new ArrayList<Contender>();

    public Sport() {
    }

    public void addContender(Integer contenderId, String contenderName) {
        logger.debug("addContender() playerId:{} playerName:{}", contenderId, contenderName);
        Contender contender = new Contender(contenderId, contenderName);
        contenders.add(contender);
    }

    public String getContendersName(Integer contenderId) {
        Contender contender = contenders.get(contenderId);
        return contender.getName();
    }


    public boolean submitPoint(Integer contenderId, Integer point) {
        logger.debug("submitPoint() points:{} contenderId:{}", point, contenderId);
        Contender contender = contenders.get(contenderId);
        logger.debug("submitPoint() contender: {}", contender);
        return contender.addBowl(point);
    }


    public Integer findPoint(Integer contenderId) {
        logger.debug("findPoint() contenderId:  {}", contenderId);
        if (contenders.size() >= (contenderId)) {
            Contender contender = contenders.get(contenderId);
            logger.debug("findPoint() contenderId:  {} point:{}", contenderId, contender.getTotalPoints());
            return contender.getTotalPoints();
        }
        return null;
    }

    public List<String> findWinner() {
        logger.debug("findWinner()");
        sportCalculator = new SportCalculator(contenders);
        return sportCalculator.determineWinner();
    }

    public Integer getTeamPoint() {
        logger.debug("getTeamPoint()");
        sportCalculator = new SportCalculator(contenders);
        return sportCalculator.determineTeamPoint();
    }

    public boolean checkNameIsUnique(String name) {
        for (Contender contender : contenders) {
                if (contender.getName().equalsIgnoreCase(name)) {
                    return false;
                }
        }
        return true;
    }

    public String getPointBoard() {
        String contenderPointBoard="";
        for (Contender contender : contenders) {
            contenderPointBoard += new ScoreBoard().getScoreBoard(contender);
        }
         return contenderPointBoard;
    }

    public void setContenders(List<Contender> contenders) {
        this.contenders = contenders;
    }


}
