package bowlingramp.contender;

public class ContenderPointException extends RuntimeException {
    public ContenderPointException(String text) {
        super(text);
    }
}
