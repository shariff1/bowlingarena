package bowlingramp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import bowlingramp.sport.Sport;
import bowlingramp.sport.SportChecker;
import bowlingramp.window.WindowPointException;

import java.io.Console;
import java.util.List;

public class BowlingAlly {
    private final static Logger logger = LoggerFactory.getLogger(BowlingAlly.class);
    private static final String LINE_BREAK = "\n";
    private final Sport sport;
    private Console console;
    private SportChecker sportChecker;
    private static final String CONTENDER_CHECKER_ERROR = "Error: Contender number greater than 0. Please try again." + LINE_BREAK;
    private static final String BALL_POINT_CHECKER_ERROR = "Error: Please enter between 1 and 10. Please try again." + LINE_BREAK;
    private static final String NAME_UNIQUE_CHECKER_ERROR = "Error: Please choose another name." + LINE_BREAK;
    private static final String NAME_INVALID_ERROR = "Error: Name invalid, Please choose another which has letters and/or numbers." + LINE_BREAK;


    public BowlingAlly() {
        sport = new Sport();
        console = System.console();
        sportChecker = new SportChecker();
    }

    public static void main(String[] args) {
        BowlingAlly bowlingAlly = new BowlingAlly();
        bowlingAlly.play();
    }


    public void play() {
        Integer numberOfContenders = getNumberOfContenders();
        setupContenders(numberOfContenders);
        playWindow(numberOfContenders);
        printWinners();
        printPoints();
        printTeamPoint();

    }

    private void printPoints() {
        console.printf(sport.getPointBoard());
    }

    private void printTeamPoint() {
        logger.debug("printTeamPoint");
        Integer teamPoint = sport.getTeamPoint();
        console.printf("Team Score is: %s %n", teamPoint);
    }

    private void setupContenders(Integer numberOfContenders) {
        for (int contenderId = 1; contenderId <= numberOfContenders; contenderId++) {

            boolean isNameUnique = false;
            boolean isInvalidName = false;
            String errorMessage = "";
            String name = null;
            while (!isInvalidName || !isNameUnique) {
                name = console.readLine("%s", errorMessage + "Name for Contender " + contenderId + "?");
                isInvalidName = sportChecker.validateName(name);
                isNameUnique = sport.checkNameIsUnique(name);
                errorMessage = determineErrorMessage(isInvalidName,isNameUnique);
            }
            sport.addContender(contenderId, name);
        }
    }

    private String determineErrorMessage(boolean invalidName, boolean isNameUnique) {
        if(!invalidName){
            return  NAME_INVALID_ERROR;
        }
        else if(!isNameUnique){
            return NAME_UNIQUE_CHECKER_ERROR;
        }
        return "";

    }

    private void playWindow(Integer numberOfContenders) {
        for (int windowNumber = 1; windowNumber <= 10; windowNumber++) {
            console.printf("Window: %s %n", windowNumber);
            logger.debug("window: {}", windowNumber);
            playWindows(numberOfContenders);
        }
    }

    private void playWindows(Integer numberOfContenders) {
        for (int contenderId = 0; contenderId < numberOfContenders; contenderId++) {
            String contenderName = sport.getContendersName(contenderId);
            logger.debug("contenderId: {}", contenderId);
            console.printf("Contender %s - %s's turn....", contenderId+1,contenderName);
            takeTurn(contenderId, contenderName);
            console.printf("%s %n %n", contenderName + " has " + sport.findPoint(contenderId) + " points");
        }
    }

    private void takeTurn(int contenderId, String contenderName) {
        logger.debug("takeTurn() contenderId:{}", contenderId);
        boolean contenderWindowFinished = false;
        while (!contenderWindowFinished) {
            Integer ballPoint = getBallPoint(contenderName);
            logger.debug("ballPoint:{}", ballPoint);
            try {
                contenderWindowFinished = sport.submitPoint(contenderId, ballPoint);
            } catch (WindowPointException windowPointException) {
                logger.error("windowPointException:"+ windowPointException.getMessage());
                console.printf("%s", windowPointException.getMessage());
            }
            logger.debug("contender completed turn - contenderId:{}", contenderId);
        }
    }

    private void printWinners() {
        logger.debug("printWinners");
        List<String> winners = sport.findWinner();
        if (winners.size() > 1) {
            console.printf("Joint winners are: %n");
            for (String winner : winners) {
                console.printf("%s %n", winner);
            }
        } else if (winners.size() == 1) {
            console.printf("The winner is: %n");
            console.printf("%s %n", winners.get(0));
        }
    }

    private Integer getNumberOfContenders() {
        logger.debug("getNumberOfContenders()");
        boolean validNumberOfContenders = false;
        String errorMessage = "";
        String numberContender = null;
        while (!validNumberOfContenders) {
            numberContender = console.readLine("%s", errorMessage + "Number of contenders? ");
            validNumberOfContenders = sportChecker.validateNumberOfContenders(numberContender);
            errorMessage = CONTENDER_CHECKER_ERROR;
        }
        return Integer.valueOf(numberContender);
    }

    private Integer getBallPoint(String contenderName) {
        logger.debug("getBallPoint() contenderName:{} errorMessage:{}", contenderName);
        boolean validBallPoint = false;
        String errorMessage = "";
        String ballPoint = null;
        while (!validBallPoint) {
            ballPoint = console.readLine("%s", errorMessage + LINE_BREAK+"How many balls did " + contenderName + " knock down:");
            validBallPoint = sportChecker.validateBallPoint(ballPoint);
            errorMessage = BALL_POINT_CHECKER_ERROR;
        }
        return Integer.valueOf(ballPoint);
    }

}

