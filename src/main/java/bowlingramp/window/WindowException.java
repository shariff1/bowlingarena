package bowlingramp.window;

public class WindowException extends RuntimeException {
    public WindowException(String text) {
        super(text);
    }
}
