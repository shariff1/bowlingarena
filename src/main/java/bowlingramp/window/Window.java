package bowlingramp.window;

public class Window {
    private final Integer windowNumber;
    private final WindowPoints windowPoints = new WindowPoints();

    public Window(Integer windowNumber) {
        this.windowNumber = windowNumber;
    }

    public boolean isWindowComplete() {
        return windowPoints.getTotalPoints() != null;
    }

    public Integer getWindowNumber() {
        return windowNumber;
    }

    public void submitBallPoint(int ballPoint) {
        if (windowNumber < 10) {
            updatePoint(ballPoint);
        } else {
            throw new IllegalArgumentException("window number > 9 - windowNumber:" + windowNumber);
        }
    }

    public boolean isWindowInPlay() {
        return !windowPoints.isABigStrike() && windowPoints.getSecondBallPoint() == null;
    }


    public WindowPoints getWindowPoints() {
        return this.windowPoints;
    }

    public boolean isWindowFinished() {
        return !isWindowInPlay();
    }

    private void updatePoint(int ballScore) {
        if (windowPoints.getTotalPoints() == null && windowPoints.getFirstBallPoint() == null) {
            windowPoints.setFirstBallPoint(ballScore);


        } else if (!isWindowComplete() && windowPoints.getSecondBallPoint() == null) {
            Integer totalResult = windowPoints.getFirstBallPoint() + ballScore;
            checkPointsForTwoBalls(totalResult);
            windowPoints.setSecondBallPoint(ballScore);

            if (totalResult < 10) {
                windowPoints.setTotalPoints(totalResult);
            }

        } else {
            throw new IllegalArgumentException("submitBallPoints when window is over");
        }
    }

    private void checkPointsForTwoBalls(int totalScore) {
        if (totalScore > 10) {
            throw new WindowPointException("Total points greater than maximum possible i.e. 10 - totalScore:" + totalScore);
        }
    }

    @Override
    public String toString() {
        return "Window{" +
                "windowNumber=" + windowNumber +
                ", windowPoints=" + windowPoints +
                ", isWindowInPlay=" + isWindowInPlay() +
                '}';
    }
}
