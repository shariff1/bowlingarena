package bowlingramp.sport;

import bowlingramp.contender.Contender;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ScoreBoardTest {

    private static final int CONTENDER_1 = 0;
    private static final int CONTENDER_2 = 1;
    private static final int CONTENDER_3 = 2;
    private static final String CONTENDER_1_NAME = "Ahmed Shariff";
    private static final String CONTENDER_2_NAME = "Gautam Kumar";
    private static final String CONTENDER_3_NAME = "Asmath somroo";
    private ScoreBoard scoreBoard;

    private String expectedPointBoard_contender1 = "\n" +
            "PLAYER Ahmed Shariff\n" +
            "___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________\n" +
            "|    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    |\n" +
            "|  1 |  1 | |  1 |  1 | |  1 |  1 | |  1 |  1 | |  1 |  1 | |  1 |  1 | |  1 |  1 | |  1 |  1 | |  1 |  1 | |  1 |  1 |\n" +
            "|    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____|\n" +
            "|         | |         | |         | |         | |         | |         | |         | |         | |         | |         |\n" +
            "|     2   | |     4   | |     6   | |     8   | |    10   | |    12   | |    14   | |    16   | |    18   | |    20   |\n" +
            "|_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________|\n" +
            "\n";

    private String expectedPointBoard_contender2 = "\n" +
            "PLAYER Gautam Kumar\n" +
            "___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________\n" +
            "|    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    |\n" +
            "|  1 |  3 | |  5 |  / | | 10 |  X | |  5 |  4 | |  2 |  1 | |  6 |  / | | 10 |  X | |  2 |  4 | |  5 |  1 | |  8 |  1 |\n" +
            "|    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____|\n" +
            "|         | |         | |         | |         | |         | |         | |         | |         | |         | |         |\n" +
            "|     4   | |    24   | |    43   | |    52   | |    55   | |    75   | |    91   | |    97   | |   103   | |   112   |\n" +
            "|_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________|\n" +
            "\n";

    private String expectedPointBoard_contender3 = "\n" +
            "PLAYER Asmath somroo\n" +
            "___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________ ___________\n" +
            "|    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    | |    |    |\n" +
            "| 10 |  X | | 10 |  X | | 10 |  X | | 10 |  X | | 10 |  X | | 10 |  X | | 10 |  X | | 10 |  X | | 10 |  X | | 10 |  X |\n" +
            "|    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____| |    |____|\n" +
            "|         | |         | |         | |         | |         | |         | |         | |         | |         | |         |\n" +
            "|    30   | |    60   | |    90   | |   120   | |   150   | |   180   | |   210   | |   240   | |   270   | |   300   |\n" +
            "|_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________| |_________|\n" +
            "\n";

    @Before
    public void setUp() throws Exception {
        scoreBoard = new ScoreBoard();
    }

    @Test
    public void testLowScores() throws Exception {
        Contender contender1 = new Contender(CONTENDER_1, CONTENDER_1_NAME);
        addPoints(contender1, 1);

        assertEquals(expectedPointBoard_contender1, scoreBoard.getScoreBoard(contender1));
    }

    @Test
    public void tesMixedScores() throws Exception {
        Contender contender2 = new Contender(CONTENDER_2, CONTENDER_2_NAME);
        addPoints(contender2, Arrays.asList(1, 3, 5, 5, 10, 5, 4, 2, 1, 6, 4, 10, 2, 4, 5, 1, 8, 1));

        assertEquals(expectedPointBoard_contender2, scoreBoard.getScoreBoard(contender2));
    }

    @Test
    public void testHighestPoint() throws Exception {
        Contender contender3 = new Contender(CONTENDER_3, CONTENDER_3_NAME);
        addPoints(contender3, Arrays.asList(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10));

        assertEquals(expectedPointBoard_contender3, scoreBoard.getScoreBoard(contender3));
    }


    private void addPoints(Contender contender, List<Integer> points) {
        for (Integer point : points) {
            contender.addBowl(point);
        }
    }

    private void addPoints(Contender contender, int ballPoint) {
        for (int i = 1; i <= 20; i++) {
            contender.addBowl(ballPoint);
        }
    }
}

